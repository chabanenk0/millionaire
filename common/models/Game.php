<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;



/**
 * This is the model class for table "game".
 *
 * @property integer $id
 * @property integer $start_game
 * @property integer $stop_game
 * @property integer $user_id
 * @property User $user
 * @property integer $current_number_question
 * @property QuestionInGame $currentQuestion
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_game', 'stop_game', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'start_game' => 'Start Game',
            'stop_game' => 'Stop Game',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getQuestionsInGame()
    {
        return $this->hasMany(QuestionInGame::className(), ['game_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'start_game',
                'updatedAtAttribute' => false,
            ]
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->user_id = Yii::$app->user->getId();

            return true;
        } else
            return false;
    }

    public function generateQuestions()
    {
        $selectedQuestionIds = [];
        for ($level = 1; $level <= 3; $level++) {
            $allQuestionsIds = Question::find()->andWhere(['level_id' => $level])->column();
            for ($numberOfQuestion = 0; $numberOfQuestion < 5; $numberOfQuestion++) {
                $arrayElement = rand(0, count($allQuestionsIds) - 1);
                $selectedQuestionIds[] = $allQuestionsIds[$arrayElement];
                unset($allQuestionsIds[$arrayElement]);
                sort($allQuestionsIds);
            }

        }
        for ($questionNumber = 0; $questionNumber < count($selectedQuestionIds);
             $questionNumber++
        ) {
            $questionInGame = new QuestionInGame();
            $questionInGame->question_id = $selectedQuestionIds[$questionNumber];
            $questionInGame->question_number = $questionNumber + 1;// numbers of quest must be 1,2,3...
            $questionInGame->save(false);
        }
        $a = 0;
    }

    public function getGeneratedQuestions()
    {
        return $this->hasMany(QuestionInGame::className(), ['game_id' => 'id']);
    }

    /**
     * @return null|QuestionInGame
     */
    public function getCurrentQuestion()
    {
        return QuestionInGame::findOne([
            'game_id' => $this->id,
            'question_number' => $this->current_number_question
        ]);
    }

    /**
     * @param bool $returnFalse
     * @return array|bool|null|\common\models\Game
     * @throws NotFoundHttpException
     */
    public static function findCurrentGame($returnFalse = false)
    {
        if (($model = Game::find(Yii::$app->user->getId())
                ->andWhere([
                    'stop_game' => null,
                    'user_id' => Yii::$app->user->getId()])
                ->one()) !== null
        ) {
            return $model;
        } else {
            if ($returnFalse)
                return false;
            else
                throw new NotFoundHttpException('Such game not found');
        }
    }

    public static function startGame()
    {
        if (!count($allQuestionsIds = Question  ::find()->column())) {
            throw new BadRequestHttpException('Service temporary not contains questions');
        }
        /* @var $game \common\models\Game */
        if ($game = self::findCurrentGame(true)) {
            if (count($game->generatedQuestions) !== 15 ||
                $game->generatedQuestions[0]->game_id != $game->id
            ) {
                QuestionInGame::deleteAll(['game_id' => $game->id]);
                $game->generateQuestions();
            }
        } else {
            $game = new Game();
            $game->save(false);
            $game->generateQuestions();
        }
    }

    /**
     * @return null|\common\models\question
     * @throws NotFoundHttpException
     */
    public static function nextQuestion()
    {
        $game = self::findCurrentGame();

        if ($game->current_number_question){

        }
        else
            $game->current_number_question = 1;
        $game->save(false);

        $question = QuestionInGame::findOne([
            'game_id' => $game->id,
            'question_number' => $game->current_number_question,
        ]);

        return $question;
    }

    public function stopGame()
    {
        $this->stop_game = time();
        $this->save();
    }

    public function gameWin()
    {
        if ($this->currentQuestion->question_number == 15) {
            self::stopGame();
            return true;
        }

        return false;
    }

    /**
     * @param $idUserAnswer
     * @return QuestionInGame
     * @throws NotFoundHttpException
     */
    public static function submitAnswer($idUserAnswer)
    {
        $game = Game::findCurrentGame();

        if ($game->currentQuestion->checkUserAnswer($idUserAnswer)) {
            if ($game->gameWin()) {
                $status = 'win';
                //$game->currentQuestion->gameStatus = 'win';
            }
        } else {
            $game->stopGame();
            //$game->currentQuestion->gameStatus = 'lost';
            $status = 'lost';
        }
        $currentQuestion = $game->currentQuestion;
        $currentQuestion->gameStatus = isset($status) ? $status : null;
        $game->current_number_question++;
        $game->save(false);

        return $currentQuestion;
    }


}
