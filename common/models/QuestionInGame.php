<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "game_question_result".
 *
 * @property integer $id
 * @property integer $question_id
 * @property integer $user_answer_id
 * @property integer $is_correct
 * @property integer $game_id
 * @property integer $question_number
 * @property Question $question

*/
class QuestionInGame extends \yii\db\ActiveRecord
{
    public $gameStatus = null; //such question contains status of stopping game | win or lost
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game_question_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [['question_id'], 'required'],
                ['question_id', 'user_answer_id', 'is_correct', 'game_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'user_answer_id' => 'User Answer ID',
            'is_correct' => 'Is Correct',
            'game_id' => 'Game ID',
        ];
    }

    public function getGame()
    {
        return $this->hasOne(Game::className(), ['id' => 'game_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $game = Game::findCurrentGame();
            $this->game_id = $game->id;
            return true;
        } else
            return false;
    }

    public function getQuestion()
    {
        return $this->hasOne(Question::className(),
            ['id' => 'question_id']);
    }

    /**
     * validate truth answer only for current question
     * @param $idAnswer
     * @return bool|int
     */
    public function checkUserAnswer($idAnswer)
    {
        $this->user_answer_id = $idAnswer;
        $this->is_correct =
            ($this->question->id_answer_true == $idAnswer) ?
                true : false;
        $this->save(false);
        return $this->is_correct;
    }


}
