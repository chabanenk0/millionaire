<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property integer $id
 * @property integer $id_answer_true
 * @property string $text_question
 * @property integer $level_id
 *
 * @property Answer[] $answers
 * @property Answer $answerTrue
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_answer_true', 'level_id'], 'integer'],
            [['text_question'], 'required'],
            [['text_question'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_answer_true' => 'Correct answer',
            'text_question' => 'Question',
            'level_id' => 'Level ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswerTrue()
    {
        return $this->hasOne(Answer::className(), ['id' => 'id_answer_true']);
    }
}