<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\QuestionLevel */

$this->title = 'Update Question Level: ' . ' ' . $model->question_number_id;
$this->params['breadcrumbs'][] = ['label' => 'Question Levels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->question_number_id, 'url' => ['view', 'id' => $model->question_number_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="question-level-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
