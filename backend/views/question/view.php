<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.01.16
 * Time: 15:07
 */
/* @var $this yii\web\View */

use yii\helpers\Html;
use common\models\Answer;

$this->title = $model->text_question;
$this->params['breadcrumbs'][] = ['label' => 'Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="post-view">
    <h1><?= $this->title ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger', 'data-confirm' => 'Are sure you want to delete this question?',
        'data-method' => 'post']) ?>
    </p>

    <?= \yii\widgets\DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text_question',
            [
                'attribute' => Answer::$division[$i = 0],
                'value' => isset($model->answers[$i]->text_answer) ?
                    $model->answers[$i]->text_answer : null
            ],
            [
                'attribute' => Answer::$division[$i = 1],
                'value' => isset($model->answers[$i]->text_answer) ?
                    $model->answers[$i]->text_answer : null
            ],
            [
                'attribute' => Answer::$division[$i = 2],
                'value' => isset($model->answers[$i]->text_answer) ?
                    $model->answers[$i]->text_answer : null
            ],
            [
                'attribute' => Answer::$division[$i = 3],
                'value' => isset($model->answers[$i]->text_answer) ?
                    $model->answers[$i]->text_answer : null
            ],
            [
                'attribute' => 'Correct answer',
                'value' => isset($model->answerTrue->text_answer) ?
                    $model->answerTrue->text_answer : null
            ],
            'level_id'
        ]
    ]);
    ?>

</div>