<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.01.16
 * Time: 17:21
 */

/* @var $question */
/* @var $answers */
/* @var $this \yii\web\View */

use yii\helpers\Html;

$this->title = 'Update question';
$this->params['breadcrumbs'][] = ['label' => 'Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= $this->title ?></h1>

<?= $this->render('_form', [
    'question' => $question,
    'answers' => $answers
]); ?>
