<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.01.16
 * Time: 15:11
 */

namespace backend\controllers;


use common\models\AddQuestionForm;
use common\models\Answer;
use common\models\Question;
use yii\base\Behavior;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class QuestionController extends Controller {


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            if (Yii::$app->user->getIdentity()) {
                                return Yii::$app->user->getIdentity()
                                    ->username == 'admin' ? true : false;
                            }
                            return false;
                        }
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ]
            ]
        ];
    }

    public function actionCreate()
    {
        $question = new Question();
        $answers = [];
        for ($i=0; $i < count(Answer::$division); $i++) {
            $answers[] = new Answer();
        }

        /** multiple loading model
         * @link http://www.yiiframework.com/doc-2.0/guide-input-tabular-input.html
         */
        $post = Yii::$app->request->post();
        if ($question->load($post) && $question->validate() && Answer::loadMultiple($answers, $post)&& Answer::validateMultiple($answers)) {

            // getting id for answers by saving
            foreach ($answers as $answer) {
                /* @var $answer \common\models\Answer*/
                $answer->save();
            }

            //if set fake foreign key of true answer - saving fail
            // $question->id_answer_true has number of sequence in array of answers
            $answer_true = $answers[$question->id_answer_true];
            unset($question->id_answer_true);
            $level_id = $question->level_id;
            unset($question->level_id);
            $question->save();

            $question->level_id = $level_id;
            $question->id_answer_true = $answer_true->id;
            $question->save();

            foreach ($answers as $answer) {
                $answer->question_id = $question->id;
                $answer->save(false);
            }

            return $this->redirect(['view', 'id' => $question->id]);

        }
        return $this->render('create', [
            'question' => $question,
            'answers' => $answers
        ]);
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Question::find(),
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    public function actionUpdate($id)
    {
        /* @var $question \common\models\Question */
        $question = $this->findModel($id);
        /* @var $answers \common\models\Answer[]*/
        $answers = $question->answers;

        $post = Yii::$app->request->post();
        if ($question->load($post) && $question->validate() && Answer::loadMultiple($answers, $post) && Answer::validateMultiple($answers)) {

            foreach ($answers as $answer) {
                $answer->save(false);
            }
            $question->id_answer_true = $answers[$question->id_answer_true]->id;
            $question->save(false);

            return $this->redirect(['view', 'id' => $question->id]);
        }

        $answersIds = array_flip(ArrayHelper::getColumn($answers, 'id'));
        $question->id_answer_true = $answersIds[$question->id_answer_true];

            return $this->render('update', [
            'question' => $question,
            'answers' => $answers,
        ]);


    }

    public function actionDelete($id)
    {
        /* @var $question \common\models\Question */
        $question = $this->findModel($id);

        /* @var $answers \common\models\Answer[] */
        $answers = $question->answers;
        $question->unlink('answerTrue', $question->answerTrue);
        // question couldn't be deleted because answers are linked with it
        // answer couldn't be deleted because question linked with every answer
        foreach ($answers as $answer) {
            $question->unlink('answers', $answer);
            $answer->unlink('question', $question);
            $answer->delete();
        }

        $question->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        $model = Question::findOne($id);
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}