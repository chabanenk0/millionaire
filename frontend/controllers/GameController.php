<?php

namespace frontend\controllers;

use common\models\Question;
use common\models\QuestionInGame;
use Yii;
use common\models\Game;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * GameController implements the CRUD actions for Game model.
 */
class GameController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all games of authorized user.
     * @return mixed
     */
    public function actionIndex()
    {
        /* @var $user \common\models\User */
        if ($user = Yii::$app->user->getIdentity()) {

            $dataProvider = new ActiveDataProvider([
                'query' => Game::find()->andWhere(['user_id' => $user->id]),
                'sort' => [
                    'defaultOrder' => ['start_game' => SORT_DESC]
                ]
            ]);

            return $this->render('index', [
                'user' => $user,
                'dataProvider' => $dataProvider
            ]);
        }

        return $this->redirect(['site/login']);
    }


    /**
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionStart()
    {
        Game::startGame();
        return $this->redirect(['next-question']);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionNextQuestion()
    {
        $question = Game::nextQuestion();
        if ($question) {
            return $this->render('view', [
                'question' => $question,
            ]);
        }
        else throw new NotFoundHttpException('Not found next question');
    }

    /**
     * @param $id
     * @return string
     */
    public function actionSubmitQuestion($id)//id answer from user
    {
        $currentQuestion = Game::submitAnswer($id);

        return $this->render('show-right-answer', [
            'currentQuestion' => $currentQuestion
        ]);
    }

    public function actionGameOver()
    {
        return $this->render('game-over');
    }

    public function actionYouWin()
    {
        return $this->render('you-win');
    }

    public function actionStopGame()
    {
        $game = Game::findCurrentGame();
        $game->stopGame();
        return $this->redirect('index');
    }
}
