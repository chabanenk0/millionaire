<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $question common\models\QuestionInGame */

$this->title = 'Current question: ' . $question->question_number;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$question = $question->question;
?>
<div class="game-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div>
        <?= $question->text_question ?>
    </div>

    <div>
        <?php foreach ($question->answers as $answer): ?>
        <p>
            <?= Html::a($answer->text_answer, ['submit-question', 'id' => $answer->id]) ?>
        </p>
        <?php endforeach ?>
    </div>

    <div><?= Html::a('Stop game', ['game/stop-game'], ['class' => 'btn btn-danger']) ?></div>

</div>
