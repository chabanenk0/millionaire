<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13.02.16
 * Time: 16:15
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $currentQuestion common\models\QuestionInGame */

$this->title = 'Current question: ' . $currentQuestion->question_number;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$question = $currentQuestion->question;
?>
<div class="game-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div>
        <?= $question->text_question ?>
    </div>

    <div>
        <?php foreach ($question->answers as $answer):
                $a = $currentQuestion->user_answer_id;
                $b = $answer->id;
                $c = 0;
            ?>
            <div
            <?= ($answer->id == $currentQuestion->user_answer_id) ? 'style="background:orange"' : '' ?>>
                <?= Html::tag(
                    'p',
                    $answer->text_answer,
                    ['style' => ($question->id_answer_true === $answer->id) ? 'color:green' : 'color:red'])
                ?>
            </div>
        <?php endforeach ?>
    </div>
    <?php
        $options = [];
        switch ($currentQuestion->gameStatus) {
        case 'win':
            $text = 'You win';
            $url = ['you-win'];
            $options = ['class' => 'btn btn-success'];
            break;
        case 'lost':
            $text  = 'Game over';
            $url = ['game-over'];
            $options = ['class' => 'btn btn-danger'];
            break;
        default:
            $text  = 'Next question';
            $url = ['next-question'];
            $options = ['class' => 'btn btn-primary'];
        } ?>

    <p>
        <?= Html::a($text, $url, $options) ?>
    </p>

</div>