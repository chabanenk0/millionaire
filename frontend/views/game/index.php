<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => \yii\grid\SerialColumn::className()],
            'start_game:dateTime',
            'stop_game:dateTime',
            'current_number_question',

        ]
    ]) ?>

    <p>
        <?= Html::a(\common\models\Game::findCurrentGame(true) ?
            'Continue game' :  'Start new game', ['start'],
            ['class' => 'btn btn-success']) ?>
    </p>


</div>
