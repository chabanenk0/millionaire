<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Game over.</h1>

        <p class="lead">You did not correctly answer the last question</p>

        <p><?= Html::a('Start new Game', ['game/start'], ['class' => 'btn btn-lg btn-success']) ?></p>
    </div>
</div>
