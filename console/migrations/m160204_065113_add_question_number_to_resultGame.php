<?php

use yii\db\Schema;
use yii\db\Migration;

class m160204_065113_add_question_number_to_resultGame extends Migration
{
    public function up()
    {
        $this->addColumn(
            \common\models\QuestionInGame::tableName(),
            'question_number', $this->integer()
        );
    }

    public function down()
    {
        $this->dropColumn(
            \common\models\QuestionInGame::tableName(),
            'question_number'
        );
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
