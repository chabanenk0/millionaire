<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_113838_question_answer extends Migration
{
    public $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

    public function up()
    {
        $this->createTable('question', [
            'id' => $this->primaryKey(),
            'id_answer_true' => $this->integer(11),
            'text_question' => $this->string()->notNull(),
            'level_id' => $this->integer(),
        ], $this->tableOptions);

        $this->createTable('answer', [
            'id' => $this->primaryKey(),
            'text_answer' => $this->string()->notNull(),
            'question_id' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'FK-question-id_answer_true',
            'question',
            'id_answer_true',
            'answer',
            'id'
            );

        $this->addForeignKey(
            'FK-answer-question_id',
            'answer',
            'question_id',
            'question',
            'id'
        );
    }

    public function down()
    {
        $this->dropForeignKey('FK-answer-question_id', 'answer');
        $this->dropForeignKey('FK-question-id_answer_true', 'question');
        $this->dropTable('question');
        $this->dropTable('answer');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
