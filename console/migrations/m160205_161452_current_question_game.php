<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_161452_current_question_game extends Migration
{
    public function up()
    {
        $this->addColumn(\common\models\Game::tableName(),
            'current_number_question',
            $this->integer()
        );
    }

    public function down()
    {
        $this->dropColumn(\common\models\Game::tableName(),
            'current_number_question'
        );
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
