<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_113158_game extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%game}}', [
            'id' => $this->primaryKey(),
            'start_game' => $this->integer(),
            'stop_game' => $this->integer(),
            'user_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('FK_game_user', 'game', 'user_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%game}}');
    }
}
