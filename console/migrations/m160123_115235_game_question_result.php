<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_115235_game_question_result extends Migration
{
    public $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

    public function up()
    {
        $this->createTable('game_question_result', [
            'id' => $this->primaryKey(),
            'question_id' => $this->integer(),
            'user_answer_id' => $this->integer(),
            'is_correct' => $this->boolean(),
            'game_id' => $this->integer(),
        ], $this->tableOptions);

    }

    public function down()
    {
        $this->dropTable('game_question_result');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
