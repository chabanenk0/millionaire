<?php

use yii\db\Schema;
use yii\db\Migration;

class m160206_112218_generate_question_levels extends Migration
{

    private function getQuestionLevels()
    {
        return [
            [
                'question_number_id' => 1,
                'level' => 1,
                'sum' => 100,
            ],
            [
                'question_number_id' => 2,
                'level' => 1,
                'sum' => 200,
            ],
            [
                'question_number_id' => 3,
                'level' => 1,
                'sum' => 300,
            ],

            [
                'question_number_id' => 4,
                'level' => 1,
                'sum' => 500,
            ],
            [
                'question_number_id' => 5,
                'level' => 2,
                'sum' => 1000,
            ],
            [
                'question_number_id' => 6,
                'level' => 2,
                'sum' => 2000,
            ],
            [
                'question_number_id' => 7,
                'level' => 2,
                'sum' => 4000,
            ],
            [
                'question_number_id' => 8,
                'level' => 2,
                'sum' => 8000,
            ],
            [
                'question_number_id' => 9,
                'level' => 2,
                'sum' => 16000,
            ],
            [
                'question_number_id' => 10,
                'level' => 3,
                'sum' => 32000,
            ],
            [
                'question_number_id' => 11,
                'level' => 3,
                'sum' => 64000,
            ],
            [
                'question_number_id' => 12,
                'level' => 3,
                'sum' => 126000,
            ],
            [
                'question_number_id' => 13,
                'level' => 3,
                'sum' => 250000,
            ],
            [
                'question_number_id' => 14,
                'level' => 3,
                'sum' => 600000,
            ],
            [
                'question_number_id' => 15,
                'level' => 3,
                'sum' => 1000000,
            ],
        ];
    }

    public function up()
    {
        foreach ($this->getQuestionLevels() as $questionLevel) {
            $this->insert('question_level', [
                'question_number_id' => $questionLevel['question_number_id'],
                'level' => $questionLevel['level'],
                'sum' => $questionLevel['sum'],
            ]);
        }
    }

    public function down()
    {
        $this->delete('question_level', ['<=', 'question_number_id', '15']);

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
