<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_120210_question_level extends Migration
{
    public $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

    public function up()
    {
        $this->createTable('question_level', [
            'question_number_id' => $this->primaryKey(),
            'level' => $this->integer(3),
            'sum' => $this->integer(),
        ], $this->tableOptions);
    }

    public function down()
    {
        $this->dropTable('question_level');
    }
}
